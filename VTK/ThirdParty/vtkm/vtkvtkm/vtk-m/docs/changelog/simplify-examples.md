# Simplify examples

Lots of the examples were out of date or way too verbose. The examples have
been simplified and brought up to modern VTK-m conventions.

We have also added a "hello worklet" example to be a minimal example of
creating a working algorithm (wrapped in a filter) in VTK-m (and used).
