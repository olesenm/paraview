Description: Reduce the error message in cmake if module is missing
Author: Anton Gladky <gladk@debian.org>
Bug-Debian: https://bugs.debian.org/783797
Last-Update: 2015-05-05

